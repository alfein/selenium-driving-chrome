"use strict";

//Update path to the driver
process.env.PATH = process.env.PATH + ";" + process.cwd() + "\\driver";

var driver = require("selenium-webdriver");

async function basicExample() {
  try {
    var capabilities = driver.Capabilities.chrome();
    capabilities.set("chromeOptions", { debuggerAddress: "127.0.0.1:9222" });

    var browser = new driver.Builder()
      .withCapabilities(capabilities)
      .forBrowser("chrome")
      .build();

    var tabs = await browser.getAllWindowHandles();
    console.log(tabs);

    await browser.switchTo().window(tabs[0]);

    browser.quit();
  } catch (err) {
    handleFailure(err, driver);
  }
}

function handleFailure(err, driver) {
  console.error("Something went wrong!\n", err.stack, "\n");
  driver.quit();
}

basicExample();
